(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.edgrid = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _menu = require('./menu');

var _menu2 = _interopRequireDefault(_menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import migrate from './migrate'

exports.default = { menu: _menu2.default };
module.exports = exports['default'];

},{"./menu":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _utils = require('./utils');

//TODO: Add null pointer error checked.
exports.default = function (navId, menuId) {
  var nav = (0, _utils.$)('#' + navId),
      menu = (0, _utils.$)('#' + menuId),
      toggleButton = (0, _utils.$)('#' + navId + '-toggle');

  function showNav() {
    nav.classList.toggle('show-menu');
  }

  function showSubMenu(e) {
    if (e.target.classList.contains('expand-submenu')) {
      e.preventDefault();
      e.target.classList.toggle('active');
      e.target.previousElementSibling.classList.toggle('show-submenu');
    }
  }

  // si el nav y toggle existen mostrar u ocultar menu
  if (nav) {
    if (toggleButton) {
      toggleButton.addEventListener('click', showNav);
    } else {
      console.error('Not found ' + navId + '-toggle Id');
    }
  } else {
    console.error('Not found ' + navId + ' Id');
  }

  if (menu) {
    // show submenus
    menu.addEventListener('click', showSubMenu);
    //
    // while (menuItemsLength--) {
    //   let menuItem = menuItems[menuItemsLength];
    //   // Detectar si un item es padre de un submenu
    //   if (menuItem.querySelector('ul') != null) {
    //     menuItem.classList.add('parent-submenu');
    //
    //
    //   }
    // }

    (0, _utils.each)((0, _utils.$)('li', menu) /* menuItems */, function (menuItem) {
      // Detectar si un item es padre de un submenu
      if (menuItem.querySelector('ul') != null) {
        menuItem.classList.add('parent-submenu');

        //Crear toggle button para submenus
        var expandSubmenu = document.createElement('div');
        expandSubmenu.classList.add('expand-submenu');
        menuItem.appendChild(expandSubmenu);
      }
    });
  } else {
    console.error('Not found ' + menuId + ' Id');
  }
};

module.exports = exports['default'];

},{"./utils":3}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.$ = $;
exports.each = each;
/**
 * Alias del método `querySelectorAll`
 *
 * @param {String} selector
 * @param {Document|HTMLElement} context
 *
 * @return {NodeList|HTMLElement}
 *
 * @private
 */
function $(selector) {
  var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

  //if (/^#[\w-]+$/.test(selector)) {
  //return context.getElementById(selector.slice(1))
  //}

  //return context.querySelectorAll(selector)

  return (/^#[\w-]+$/.test(selector) ? context.getElementById(selector.slice(1)) : context.querySelectorAll(selector)
  );
}

/**
 * Recorre todos los elementos de un NodeList o HTMLCollection.
 *
 * @param {NodeList|HTMLCollection} elements
 * @param {Function} callback
 *
 * @return void
 *
 * @private
 */
function each(elements, callback) {
  var length = elements.length;


  for (var i = 0; i < length; ++i) {
    callback(elements[i], i, elements);
  }
}

},{}]},{},[1])(1)
});
let wow = new WOW(
  {
    boxClass: 'wow',      // default
    animateClass: 'animated', // default
    offset: 0,          // default
    mobile: false,       // default
    live: false        // default
  }
);
wow.init();
/* MENU MOVIL**/
edgrid.menu('main-nav','main-menu');
/* Sticky aside-menu*/
const resize = () => {
  let sticky= null;
  if (innerWidth > 800) {
    setTimeout(() => {
      sticky = new Sticky('.main-nav');
      window.scrollBy(0, -1);
    }, 2500);
  }
  if (innerWidth <= 800){
    sticky = null;
  }
}
addEventListener('resize', resize);
addEventListener('DOMContentLoaded', resize);
$(document).ready(function () {
  $('#ir-arriba').hide();
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 250) {
        $('#ir-arriba').fadeIn();
      } else {
        $('#ir-arriba').fadeOut();
      }
    })
  })
  $('#ir-arriba').click(function(){
    $('body, html').animate({
      scrollTop: '0px'
    });
  });
    var headerHeight = 10;
    $(window).resize(function () {
      console.log("enter to window resize");
      if ($(window).width() <= 960) {
        console.log("less than 960");
        headerHeight = 10;
      }
    });
    function getHeaderHeight() {
      var winWidth = $(window).width();
      console.log("called function and winWidth: " + winWidth);
      if (winWidth > 960) { headerHeight = 15; }
      if (winWidth <= 960) { headerHeight = 15; }
      if (winWidth <= 700) { headerHeight = 15; }
      if (winWidth <= 500) { headerHeight = 15; }
      if (winWidth <= 450) { headerHeight = 15; }
      if (winWidth <= 350) { headerHeight = 15; }
    }
    $(".submenu__link").click(function () {
      if (this.hash) {
        var hash = this.hash.substr(1);
        console.log("hash:" + hash);
        var $scrollToElement;
        $scrollToElement = $("*[id=" + hash + "]");
        getHeaderHeight();
        console.log("headerHeight used:" + headerHeight);
        //var headerHeight = $('header').height();
        //console.log("headerHeight:"+headerHeight);
        var scrollToPosition = $scrollToElement.offset().top - headerHeight;
        $("html, body").animate({
          scrollTop: scrollToPosition
        }, 700, "swing", function () {
          // setTimeout(function () {
          //   // window.scrollBy(0, -1);
          // }, 400)
        });
        /* To add/remove class */
        $('.menuItem').removeClass('selected'); // first remove class from all menu items
        $(this).children('.menuItem').addClass('selected'); // then add to the clicked item
        return false;
      }
    });
});
